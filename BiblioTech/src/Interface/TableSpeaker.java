/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import controller.CtrlLoans;
import domain.CD;
import domain.Speaker;
import domain.Student;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author geovanny
 */
public class TableSpeaker extends javax.swing.JPanel {

    private CtrlLoans ctrlLoans;
    private TableRowSorter tableRowSorter = new TableRowSorter();
    private int key = 1;
    private Student student;

    /**
     * Creates new form TableSpeaker
     */
    public TableSpeaker(CtrlLoans ctrlLoans, Student student) {
        initComponents();
        this.ctrlLoans = ctrlLoans;
        this.student = student;
        fillTable();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tableCD_fitro = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.X_AXIS));

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel1.setText("Filtro:");
        jPanel1.add(jLabel1);

        tableCD_fitro.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        tableCD_fitro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tableCD_fitroKeyTyped(evt);
            }
        });
        jPanel1.add(tableCD_fitro);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Título", "Código" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        jPanel1.add(jComboBox1);

        jButton1.setText("Prestar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableCD_fitroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableCD_fitroKeyTyped
        tableCD_fitro.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (tableCD_fitro.getText());
                tableCD_fitro.setText(cadena);
                repaint();
                filtro();
            }
        });
        tableRowSorter = new TableRowSorter(jTable1.getModel());
        jTable1.setRowSorter(tableRowSorter);
    }//GEN-LAST:event_tableCD_fitroKeyTyped

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        if (jComboBox1.getSelectedItem().toString().equals("Título")) {
            key = 1;
        } else if (jComboBox1.getSelectedItem().toString().equals("Código")) {
            key = 0;
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ctrlLoans.lend(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 0)), this.student);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField tableCD_fitro;
    // End of variables declaration//GEN-END:variables

    public void filtro() {
        tableRowSorter.setRowFilter(RowFilter.regexFilter(tableCD_fitro.getText().toString(), key));
    }

    public void fillTable() {
        if (ctrlLoans == null) {
            System.out.println("controlador nl");;
        }
        ArrayList<Speaker> cds = ctrlLoans.selectSpeakers();
        String headboard[] = {"ID", "Marca", "Lenguaje", "Ubicación"};
        String data[][] = {};
        DefaultTableModel model = new DefaultTableModel(data, headboard);
        jTable1.setModel(model);
        for (int i = 0; i < cds.size(); i++) {
            Object cd[] = {cds.get(i).getId(),
                cds.get(i).getBrand(),
                cds.get(i).getLanguage(),
                cds.get(i).getUbicacionCode()
            };
            model.addRow(cd);
        }
    }
}
