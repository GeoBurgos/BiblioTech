/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import controller.CtrlRegisters;
import java.awt.Component;
import java.awt.Image;
import java.awt.Rectangle;
import javafx.scene.layout.Pane;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

/**
 *
 * @author geovanny
 */
public class RegisterMaterial extends javax.swing.JFrame {

    private CtrlRegisters ctrlRegisterMaterial;
    /**
     * Creates new form RegisterMaterial
     */
    public RegisterMaterial(CtrlRegisters ctrlRegisterMaterial) {
        this.ctrlRegisterMaterial = ctrlRegisterMaterial;
        initComponents();
        jLabel6.setIcon(new ImageIcon((new ImageIcon(getClass().getResource("/Images/registerStudent.jpeg"))).getImage().getScaledInstance(jLabel6.getWidth(), jLabel6.getHeight(), Image.SCALE_DEFAULT)));        
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setTitle("Menú principal");
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
    }

    private RegisterMaterial() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setOpaque(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 521, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 368, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 71, -1, -1));

        jComboBox1.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Libros", "Audiovisuales" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 21, -1, -1));

        jComboBox2.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sin especificar" }));
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(243, 21, -1, -1));
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 550, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        if (jComboBox1.getSelectedItem().toString().equals("Libros")) {            
            jComboBox2.removeAllItems();
            jComboBox2.addItem("Libro físico");
            jComboBox2.addItem("Libro digital");
        } else if (jComboBox1.getSelectedItem().toString().equals("Audiovisuales")) {            
            jComboBox2.removeAllItems();
            jComboBox2.addItem("Laptop");
            jComboBox2.addItem("Proyector");
            jComboBox2.addItem("Parlante");
            jComboBox2.addItem("CD");
            jComboBox2.addItem("DVD");
        }       
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
        if (jComboBox2.getSelectedItem() == null) {
            
        } else if (jComboBox2.getSelectedItem().toString().equals("Libro físico")) {            
            JPanel fisicBook = new ViewFisicBook(ctrlRegisterMaterial);            
            jPanel1.removeAll();
            fisicBook.setVisible(true);            
            fisicBook.setBounds(new Rectangle(jPanel1.getPreferredSize()));
            jPanel1.add(fisicBook);
            jPanel1.updateUI();
        } else if (jComboBox2.getSelectedItem().toString().equals("Libro digital")) {
            JPanel fisicBook = new ViewDigitalBook(ctrlRegisterMaterial);
            jPanel1.removeAll();
            fisicBook.setVisible(true);
            fisicBook.setBounds(new Rectangle(jPanel1.getPreferredSize()));
            jPanel1.add(fisicBook);
            jPanel1.updateUI();
        } else if (jComboBox2.getSelectedItem().toString().equals("Laptop")) {
            JPanel fisicBook = new ViewLaptop(ctrlRegisterMaterial);
            jPanel1.removeAll();
            fisicBook.setVisible(true);
            fisicBook.setBounds(new Rectangle(jPanel1.getPreferredSize()));
            jPanel1.add(fisicBook);
            jPanel1.updateUI();
        } else if (jComboBox2.getSelectedItem().toString().equals("Proyector")) {
            JPanel fisicBook = new ViewProyector(ctrlRegisterMaterial);
            jPanel1.removeAll();
            fisicBook.setVisible(true);
            fisicBook.setBounds(new Rectangle(jPanel1.getPreferredSize()));
            jPanel1.add(fisicBook);
            jPanel1.updateUI();
        } else if (jComboBox2.getSelectedItem().toString().equals("Parlante")) {
            JPanel fisicBook = new ViewSpeaker(ctrlRegisterMaterial);
            jPanel1.removeAll();
            fisicBook.setVisible(true);
            fisicBook.setBounds(new Rectangle(jPanel1.getPreferredSize()));
            jPanel1.add(fisicBook);
            jPanel1.updateUI();
        } else if (jComboBox2.getSelectedItem().toString().equals("CD")) {
            JPanel fisicBook = new ViewCD(ctrlRegisterMaterial);
            jPanel1.removeAll();
            fisicBook.setVisible(true);
            fisicBook.setBounds(new Rectangle(jPanel1.getPreferredSize()));
            jPanel1.add(fisicBook);
            jPanel1.updateUI();
        } else if (jComboBox2.getSelectedItem().toString().equals("DVD")) {
            JPanel fisicBook = new ViewDVD(ctrlRegisterMaterial);
            jPanel1.removeAll();
            fisicBook.setVisible(true);
            fisicBook.setBounds(new Rectangle(jPanel1.getPreferredSize()));
            jPanel1.add(fisicBook);
            jPanel1.updateUI();
        }
    }//GEN-LAST:event_jComboBox2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegisterMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegisterMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegisterMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegisterMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegisterMaterial().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
