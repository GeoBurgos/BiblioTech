/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import controller.CtrlRegisters;
import domain.Language;
import domain.Proyector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author geovanny
 */
public class ViewProyector extends javax.swing.JPanel {

    private CtrlRegisters ctrlRegisterMaterial;
    
    /**
     * Creates new form Proyector
     */
    public ViewProyector(CtrlRegisters ctrlRegisterMaterial) {
        initComponents();
        this.ctrlRegisterMaterial = ctrlRegisterMaterial;
        registerMaterial_Proyector_language.setModel(new DefaultComboBoxModel<>(Language.values()));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        registerMaterial_Proyector_id = new javax.swing.JTextField();
        registerMaterial_Proyector_brand = new javax.swing.JTextField();
        registerMaterial_Proyector_ubicationCode = new javax.swing.JTextField();
        registerMaterial_Proyector_language = new javax.swing.JComboBox<>();
        registerMaterial_Proyector_lumens = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.Y_AXIS));

        jLabel3.setFont(new java.awt.Font("Ubuntu", 0, 27)); // NOI18N
        jLabel3.setText("Id:");
        jPanel1.add(jLabel3);

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 27)); // NOI18N
        jLabel4.setText("Marca:");
        jPanel1.add(jLabel4);

        jLabel2.setFont(new java.awt.Font("Ubuntu", 0, 27)); // NOI18N
        jLabel2.setText("Cód. de ubicación:");
        jPanel1.add(jLabel2);

        jLabel5.setFont(new java.awt.Font("Ubuntu", 0, 27)); // NOI18N
        jLabel5.setText("Lenguaje:");
        jPanel1.add(jLabel5);

        jLabel6.setFont(new java.awt.Font("Ubuntu", 0, 27)); // NOI18N
        jLabel6.setText("Lúmenes:");
        jPanel1.add(jLabel6);

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 230, 170));

        jPanel2.setOpaque(false);
        jPanel2.setLayout(new javax.swing.BoxLayout(jPanel2, javax.swing.BoxLayout.Y_AXIS));

        registerMaterial_Proyector_id.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jPanel2.add(registerMaterial_Proyector_id);

        registerMaterial_Proyector_brand.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        registerMaterial_Proyector_brand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerMaterial_Proyector_brandActionPerformed(evt);
            }
        });
        jPanel2.add(registerMaterial_Proyector_brand);

        registerMaterial_Proyector_ubicationCode.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        registerMaterial_Proyector_ubicationCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerMaterial_Proyector_ubicationCodeActionPerformed(evt);
            }
        });
        jPanel2.add(registerMaterial_Proyector_ubicationCode);

        registerMaterial_Proyector_language.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jPanel2.add(registerMaterial_Proyector_language);

        registerMaterial_Proyector_lumens.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jPanel2.add(registerMaterial_Proyector_lumens);

        add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 20, 240, 170));

        jPanel3.setOpaque(false);
        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.X_AXIS));

        jButton1.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        jButton1.setText("Atras");
        jButton1.setMaximumSize(new java.awt.Dimension(180, 40));
        jButton1.setMinimumSize(new java.awt.Dimension(180, 40));
        jButton1.setPreferredSize(new java.awt.Dimension(180, 40));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton1);

        jButton2.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        jButton2.setText("Registrar");
        jButton2.setMaximumSize(new java.awt.Dimension(180, 40));
        jButton2.setMinimumSize(new java.awt.Dimension(180, 40));
        jButton2.setPreferredSize(new java.awt.Dimension(180, 40));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton2);

        add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, 360, 60));
    }// </editor-fold>//GEN-END:initComponents

    private void registerMaterial_Proyector_brandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerMaterial_Proyector_brandActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_registerMaterial_Proyector_brandActionPerformed

    private void registerMaterial_Proyector_ubicationCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerMaterial_Proyector_ubicationCodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_registerMaterial_Proyector_ubicationCodeActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            Proyector proyector = new Proyector(true, registerMaterial_Proyector_ubicationCode.getText().toString(), (Language)registerMaterial_Proyector_language.getSelectedItem(), Integer.parseInt(registerMaterial_Proyector_id.getText().toString()), registerMaterial_Proyector_brand.getText().toString(), Integer.parseInt(registerMaterial_Proyector_lumens.getText().toString()));            
            if (ctrlRegisterMaterial.AddMaterial(proyector)) {
                registerMaterial_Proyector_brand.setText("");
                registerMaterial_Proyector_id.setText("");
                registerMaterial_Proyector_lumens.setText("");
                registerMaterial_Proyector_ubicationCode.setText("");
            } else {
                JOptionPane.showMessageDialog(this, "Ya existe");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Algún parser incorrecto. Error: "+e);
        }
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    public javax.swing.JTextField registerMaterial_Proyector_brand;
    public javax.swing.JTextField registerMaterial_Proyector_id;
    public javax.swing.JComboBox<Language> registerMaterial_Proyector_language;
    public javax.swing.JTextField registerMaterial_Proyector_lumens;
    public javax.swing.JTextField registerMaterial_Proyector_ubicationCode;
    // End of variables declaration//GEN-END:variables
}
