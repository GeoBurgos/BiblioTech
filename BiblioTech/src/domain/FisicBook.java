/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author geovanny
 */
public class FisicBook extends Book{
    private Condition condition;    

    /**
     * 
     */
    public FisicBook(){
        super();
    }
    
    /**
     * 
     * @param status
     * @param ubicationCode
     * @param language
     * @param barCode
     * @param ISBN
     * @param numOfPages
     * @param title
     * @param edition
     * @param condition 
     */
    public FisicBook(Boolean status, String ubicationCode, Language language, String ISBN, int numOfPages, String title, int edition, Condition condition){
        super(status, ubicationCode, language, ISBN, numOfPages, title, edition);
        this.condition = condition;
    }
    /**
     * @return the condition
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * @param condition the condition to set
     */
    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    @Override
    public String toString() {
        return "FisicBook{" + "condition=" + condition + " " + super.toString() + " " + '}';
    }
    
    
}
