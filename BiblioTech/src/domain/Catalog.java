/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;

/**
 *
 * @author geovanny
 */
public class Catalog {
    private ArrayList<Material> materials = new ArrayList<>();

    /**
     * @return the materials
     */
    public ArrayList<Material> getMaterials() {
        return materials;
    }

    /**
     * @param materials the materials to set
     */
    public void setMaterials(ArrayList<Material> materials) {
        this.materials = materials;
    }
    
    /**
     * Obtener todos los CDs disponibles del catálogo
     * @return 
     */
    public ArrayList<CD> getCDs() {
        ArrayList<CD> cds = new ArrayList<>();
        for (int i = 0; i < materials.size(); i++) {
            if(materials.get(i) instanceof CD && materials.get(i).getStatus()){
                cds.add((CD) materials.get(i));
            }
        }
        return cds;
    }
    
    /**
     * Obtener todos los libros físicos disponibles del catálogo
     * @return 
     */
    public ArrayList<FisicBook> getFisicBooks() {
        ArrayList<FisicBook> fisicBooks = new ArrayList<>();
        for (int i = 0; i < materials.size(); i++) {            
            if(materials.get(i) instanceof FisicBook && materials.get(i).getStatus()){
                fisicBooks.add((FisicBook) materials.get(i));
            }
        }
        return fisicBooks;
    }
    
    /**
     * Obtener todos los libros digitales disponibles del catálogo
     * @return 
     */
    public ArrayList<DigitalBook> getDigitalBooks() {
        ArrayList<DigitalBook> digitalBooks = new ArrayList<>();
        for (int i = 0; i < materials.size(); i++) {
            if(materials.get(i) instanceof DigitalBook && materials.get(i).getStatus()){
                digitalBooks.add((DigitalBook) materials.get(i));
            }
        }
        return digitalBooks;
    }
    
    /**
     * Obtener todos los DVDs disponibles del catálogo
     * @return 
     */
    public ArrayList<DVD> getDVDs() {
        ArrayList<DVD> dvds = new ArrayList<>();
        for (int i = 0; i < materials.size(); i++) {
            if(materials.get(i) instanceof DVD && materials.get(i).getStatus()){
                dvds.add((DVD) materials.get(i));
            }
        }
        return dvds;
    }
    
    /**
     * Obtener todos los laptops disponibles del catálogo
     * @return 
     */
    public ArrayList<Laptop> getLaptops() {
        ArrayList<Laptop> laptops = new ArrayList<>();
        for (int i = 0; i < materials.size(); i++) {
            if(materials.get(i) instanceof Laptop && materials.get(i).getStatus()){
                laptops.add((Laptop) materials.get(i));
            }
        }
        return laptops;
    }
    
    /**
     * Obtener todos los proyectores disponibles del catálogo
     * @return 
     */
    public ArrayList<Proyector> getProyectors() {
        ArrayList<Proyector> proyectors = new ArrayList<>();
        for (int i = 0; i < materials.size(); i++) {
            if(materials.get(i) instanceof Proyector && materials.get(i).getStatus()){
                proyectors.add((Proyector) materials.get(i));
            }
        }
        return proyectors;
    }
    
    /**
     * Obtener todos los parlantes disponibles del catálogo
     * @return 
     */
    public ArrayList<Speaker> getSpeakers() {
        ArrayList<Speaker> speakers = new ArrayList<>();
        for (int i = 0; i < materials.size(); i++) {
            if(materials.get(i) instanceof Speaker && materials.get(i).getStatus()){
                speakers.add((Speaker) materials.get(i));
            }
        }
        return speakers;
    }
}
