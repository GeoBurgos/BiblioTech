/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author geovanny
 */
public class CD extends Audiovisual{
    private String title;

    /**
     * 
     */
    public CD() {
        super();
    }
    
    /**
     * 
     * @param status
     * @param ubicationCode
     * @param language
     * @param id
     * @param title 
     */
    public CD(Boolean status, String ubicationCode, Language language, int id, String title) {
        super(status, ubicationCode, language, id);
        this.title = title;
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CD{" + "title=" + title + " " + super.toString() + " " + '}';
    }

    
}
