/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author geovanny
 */
public class Speaker extends Audiovisual{
    private String brand;
    private Boolean bluetooth;

    /**
     * 
     */
    public Speaker(){
        super();
    }
    
    /**
     * 
     * @param status
     * @param ubicationCode
     * @param language
     * @param id
     * @param brand
     * @param bluetooth 
     */
    public Speaker(Boolean status, String ubicationCode, Language language, int id, String brand, Boolean bluetooth){
        super(status, ubicationCode, language, id);
        this.bluetooth = bluetooth;
        this.brand = brand;
    }
    
    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the bluetooth
     */
    public Boolean getBluetooth() {
        return bluetooth;
    }

    /**
     * @param bluetooth the bluetooth to set
     */
    public void setBluetooth(Boolean bluetooth) {
        this.bluetooth = bluetooth;
    }

    @Override
    public String toString() {
        return "Speaker{" + "brand=" + brand + ", bluetooth=" + bluetooth + " " + super.toString() + " " + '}';
    }
    
    
}
