/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author geovanny
 */
public class Book extends Material{
    private String ISBN;
    private int numOfPages;
    private String title;
    private int edition;

    /**
     * 
     */
    public Book(){
        super();
    }
    
    /**
     * 
     * @param status
     * @param ubicationCode
     * @param language
     * @param barCode
     * @param ISBN
     * @param numOfPages
     * @param title
     * @param edition 
     */
    public Book(Boolean status, String ubicationCode, Language language, String ISBN, int numOfPages, String title, int edition){
        super(status, ubicationCode, language);        
        this.ISBN = ISBN;
        this.numOfPages = numOfPages;
        this.title = title;
        this.edition = edition;
    }
        
    /**
     * @return the ISBN
     */
    public String getISBN() {
        return ISBN;
    }

    /**
     * @param ISBN the ISBN to set
     */
    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    /**
     * @return the numOfPages
     */
    public int getNumOfPages() {
        return numOfPages;
    }

    /**
     * @param numOfPages the numOfPages to set
     */
    public void setNumOfPages(int numOfPages) {
        this.numOfPages = numOfPages;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the edition
     */
    public int getEdition() {
        return edition;
    }

    /**
     * @param edition the edition to set
     */
    public void setEdition(int edition) {
        this.edition = edition;
    }
    
    @Override
    public String toString() {
        return "Book{ ISBN=" + ISBN + ", numOfPages=" + numOfPages + ", title=" + title + ", edition=" + edition + " " + super.toString() + " " + '}';
    }
    
    
}
