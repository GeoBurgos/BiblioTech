/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author geovanny
 */
public class DigitalBook extends Book{
    private String URL;

    /**
     * 
     */
    public DigitalBook(){
        super();
    }
    
    /**
     * 
     * @param status
     * @param ubicationCode
     * @param language
     * @param barCode
     * @param ISBN
     * @param numOfPages
     * @param title
     * @param edition
     * @param URL 
     */
    public DigitalBook(Boolean status, String ubicationCode, Language language, String ISBN, int numOfPages, String title, int edition, String URL){
        super(status, ubicationCode, language, ISBN, numOfPages, title, edition);
        this.URL = URL;
    }        
    
    /**
     * @return the URL
     */
    public String getURL() {
        return URL;
    }

    /**
     * @param URL the URL to set
     */
    public void setURL(String URL) {
        this.URL = URL;
    }

    @Override
    public String toString() {
        return "DigitalBook{" + "URL=" + URL + " " + super.toString() + " " + '}';
    }
    
    
}
