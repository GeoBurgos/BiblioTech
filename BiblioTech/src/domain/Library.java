/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author geovanndefeaty
 */
public class Library {
    
    private static Library library;
    
    private String name = "TEC";
    private String address = "Cartago";
    private Date startTime;
    private Date endTime;
    private int mulct = 2000;
    private int loanDays = 8;
    private String password;
    private ArrayList<Student> students = new ArrayList<>();
    private Catalog catalog = new Catalog();
    private ArrayList<Loan> loans = new ArrayList<>();

    /**
     * 
     */
    public Library() {
        
    }    
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the mulct
     */
    public int getMulct() {
        return mulct;
    }

    /**
     * @param mulct the mulct to set
     */
    public void setMulct(int mulct) {
        this.mulct = mulct;
    }

    /**
     * @return the loanDays
     */
    public int getLoanDays() {
        return loanDays;
    }

    /**
     * @param loanDays the loanDays to set
     */
    public void setLoanDays(int loanDays) {
        this.loanDays = loanDays;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the students
     */
    public ArrayList<Student> getStudents() {
        return students;
    }

    /**
     * @param students the students to set
     */
    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    /**
     * @return the catalog
     */
    public Catalog getCatalog() {
        return catalog;
    }

    /**
     * @param catalog the catalog to set
     */
    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    /**
     * @return the loans
     */
    public ArrayList<Loan> getLoans() {
        return loans;
    }

    /**
     * @param loans the loans to set
     */
    public void setLoans(ArrayList<Loan> loans) {
        this.loans = loans;
    }    
    
    /**
     * Buscar un estudiante entre la lista de estudiantes de la biblioteca por su identificación
     * @param id
     * @return 
     */
    public Student findStudent(String id) {
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getId().equals(id)) {
                return students.get(i);
            }
        }
        return null;
    }
    
    /**
     * Buscar un material entre la lista de materiales de la biblioteca por su identificación
     * @param id
     * @return 
     */
    public Material findMaterial(String id) {
        for (int i = 0; i < catalog.getMaterials().size(); i++) {
            if (catalog.getMaterials().get(i) instanceof Audiovisual) {
                Audiovisual audiovisual = (Audiovisual) catalog.getMaterials().get(i);
                if (audiovisual.getId() == Integer.parseInt(id)) {
                    return catalog.getMaterials().get(i);
                }
            } else {
                Book book = (Book) catalog.getMaterials().get(i);
                if (book.getISBN().equals(id)) {
                    return catalog.getMaterials().get(i);
                }
            }
        }
        return null;
    }
    
    //Métodos solo de prueba
    public void CargarDatos() {
        catalog.getMaterials().add(new FisicBook(true, "1", Language.ALEMAN, "YWR352", 122, "Automatas", 1, Condition.MUY_BUENO));
        catalog.getMaterials().add(new FisicBook(true, "13", Language.ALEMAN, "RTE352", 1213, "POO", 1, Condition.MUY_BUENO));
        catalog.getMaterials().add(new DigitalBook(true, "2", Language.INGLES, "JGF413", 123, "Lenguajes", 2, "www.a.com"));
        catalog.getMaterials().add(new DigitalBook(true, "25", Language.INGLES, "JGF413", 1233, "Lenguajes", 2, "www.a.com"));
        catalog.getMaterials().add(new CD(true, "3", Language.PORTUGUEZ, 41234, "Leyendas"));
        catalog.getMaterials().add(new CD(true, "34", Language.ESPAÑOL, 82421, "Programación"));
        catalog.getMaterials().add(new DVD(Boolean.TRUE, "4", Language.INGLES, 342, "Arboles"));
        catalog.getMaterials().add(new DVD(Boolean.TRUE, "42", Language.ALEMAN, 312, "Grafos"));
        catalog.getMaterials().add(new Laptop(true, "5", Language.ESPAÑOL, 941, 1000, "Laptop-001"));
        catalog.getMaterials().add(new Laptop(true, "53", Language.ESPAÑOL, 231, 1000, "Laptop-002"));
        students.add(new Student("1", "Geovanny", "Burgos", Career.MECATRONICA, (Date) new Date()));
    }

    public void print(){
        for (int i = 0; i < catalog.getMaterials().size(); i++) {
            System.out.println(catalog.getMaterials().get(i).toString());
        }        
    }
    
    
}
