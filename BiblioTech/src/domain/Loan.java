/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author geovanny
 */
public class Loan {

    private int id;
    private Date dateLoan;
    private Date dateReturn;
    private Student student;
    private Material material;

    public Loan(int id, Date dateLoan, Date dateReturn, Student student, Material material) {
        this.dateLoan = dateLoan;
        this.id = id;
        this.dateReturn = dateReturn;
        this.student = student;
        this.material = material;
    }

    /**
     * @return the dateLoan
     */
    public Date getDateLoan() {
        return dateLoan;
    }

    /**
     * @param dateLoan the dateLoan to set
     */
    public void setDateLoan(Date dateLoan) {
        this.dateLoan = dateLoan;
    }

    /**
     * @return the dateReturn
     */
    public Date getDateReturn() {
        return dateReturn;
    }

    /**
     * @param dateReturn the dateReturn to set
     */
    public void setDateReturn(Date dateReturn) {
        this.dateReturn = dateReturn;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Loan{" + "id=" + id + ", dateLoan=" + dateLoan + ", dateReturn=" + dateReturn + '}';
    }

    /**
     * @return the student
     */
    public Student getStudent() {
        return student;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(Student student) {
        this.student = student;
    }

    /**
     * @return the material
     */
    public Material getMaterial() {
        return material;
    }

    /**
     * @param material the material to set
     */
    public void setMaterial(Material material) {
        this.material = material;
    }
        
}
