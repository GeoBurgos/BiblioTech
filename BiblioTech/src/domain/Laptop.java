/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author geovanny
 */
public class Laptop extends Audiovisual{
    private int memory;
    private String brand;

    /**
     * 
     */
    public Laptop(){
        super();
    }
    
    /**
     * 
     * @param status
     * @param ubicationCode
     * @param language
     * @param id
     * @param memory
     * @param brand 
     */
    public Laptop(Boolean status, String ubicationCode, Language language, int id, int memory, String brand){
        super(status, ubicationCode, language, id);
        this.memory = memory;
        this.brand = brand;
    }
    
    /**
     * @return the memory
     */
    public int getMemory() {
        return memory;
    }

    /**
     * @param memory the memory to set
     */
    public void setMemory(int memory) {
        this.memory = memory;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Laptop{" + "memory=" + memory + ", brand=" + brand + " " + super.toString() + " " + '}';
    }
    
    
}
