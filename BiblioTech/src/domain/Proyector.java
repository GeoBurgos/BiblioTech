/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author geovanny
 */
public class Proyector extends Audiovisual{
    private String brand;
    private int lumens;

    /**
     * 
     */
    public Proyector(){
        super();
    }
    
    /**
     * 
     * @param status
     * @param ubicationCode
     * @param language
     * @param id
     * @param brand
     * @param lumens 
     */
    public Proyector(Boolean status, String ubicationCode, Language language, int id, String brand, int lumens){
        super(status, ubicationCode, language, id);
        this.brand = brand;
        this.lumens = lumens;
    }
    
    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the lumens
     */
    public int getLumens() {
        return lumens;
    }

    /**
     * @param lumens the lumens to set
     */
    public void setLumens(int lumens) {
        this.lumens = lumens;
    }

    @Override
    public String toString() {
        return "Proyector{" + "brand=" + brand + ", lumens=" + lumens + " " + super.toString() + " " + '}';
    }
   
}
