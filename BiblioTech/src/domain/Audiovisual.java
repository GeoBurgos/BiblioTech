/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author geovanny
 */
public class Audiovisual extends Material{
    private int id;

    /**
     * 
     */
    public Audiovisual(){
        super();
    }
    
    /**
     * 
     * @param status
     * @param ubicationCode
     * @param language
     * @param id 
     */
    public Audiovisual(Boolean status, String ubicationCode, Language language, int id) {
        super(status, ubicationCode, language);
        this.id = id;
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Audiovisual{" + "id=" + id + " " + super.toString() + " " + '}';
    }
    
    
}
