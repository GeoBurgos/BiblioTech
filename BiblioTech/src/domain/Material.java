/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author geovanny
 */
public class Material {
    protected Boolean status;
    protected String ubicationCode;
    protected Language language;

    /**
     * 
     */
    public Material(){}
    
    /**
     * 
     * @param status
     * @param ubicationCode
     * @param language 
     */
    public Material(Boolean status, String ubicationCode, Language language){
        this.status = status;
        this.ubicationCode = ubicationCode;
        this.language = language;
    }
    
    /**
     * @return the status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return the ubicacionCode
     */
    public String getUbicacionCode() {
        return ubicationCode;
    }

    /**
     * @param ubicacionCode the ubicacionCode to set
     */
    public void setUbicacionCode(String ubicacionCode) {
        this.ubicationCode = ubicacionCode;
    }

    /**
     * @return the language
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "Material{" + "status=" + status + ", ubicationCode=" + ubicationCode + ", language=" + language + '}';
    }
    
    
}
