/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Audiovisual;
import domain.Book;
import domain.Library;
import domain.Material;
import domain.Student;

/**
 *
 * @author geovanny
 */
public class CtrlRegisters {

    private Library library;

    /**
     *
     * @param library
     */
    public CtrlRegisters(Library library) {
        this.library = library;
    }

    /**
     * Envia un objeto material recibido de la interfaz al modelo para que se guarde en la lista de materiales de la biblioteca
     * @param material
     */
    public Boolean AddMaterial(Material material) {
        try {
            if (material instanceof Audiovisual) {
                Audiovisual audiovisual = (Audiovisual) material;
                if (library.findMaterial(String.valueOf(audiovisual.getId())) == null) {
                    library.getCatalog().getMaterials().add(material);
                    return true;
                }
            } else {
                Book book = (Book) material;
                if (library.findMaterial(book.getISBN()) == null) {
                    library.getCatalog().getMaterials().add(material);
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
    
    /**
     * Envia un objeto estudiante recibido de la interfaz al modelo para que se guarde en la lista de materiales de la biblioteca
     * @param student
     */
    public Boolean AddStudent(Student student) {
        if (library.findStudent(student.getId()) != null) {
            library.getStudents().add(student);
            return true;
        }
        return false;
    }    
}
