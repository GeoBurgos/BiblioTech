/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.CD;
import domain.DVD;
import domain.DigitalBook;
import domain.FisicBook;
import domain.Laptop;
import domain.Library;
import domain.Loan;
import domain.Proyector;
import domain.Speaker;
import domain.Student;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author geovanny
 */
public class CtrlLoans {
    
    private Library library;

    public CtrlLoans(Library library) {
        this.library = library;
    }
    
    /**
     * Trae lista de Libros fisicas disponibles de la biblioteca
     * @return 
     */
    public ArrayList<FisicBook> selectFisicBooks() {
        return library.getCatalog().getFisicBooks();
    }
    
    /**
     * Trae lista de libros digitales disponibles de la biblioteca
     * @return 
     */
    public ArrayList<DigitalBook> selectDigitalBooks() {
        return library.getCatalog().getDigitalBooks();
    }
    
    /**
     * Trae lista de laptops disponibles de la biblioteca
     * @return 
     */
    public ArrayList<Laptop> selectLaptops() {
        return library.getCatalog().getLaptops();
    }
    
    /**
     * Trae lista de parlantes disponibles de la biblioteca
     * @return 
     */
    public ArrayList<Speaker> selectSpeakers() {
        return library.getCatalog().getSpeakers();
    }
    
    /**
     * Trae lista de cds disponibles en la biblioteca
     * @return 
     */
    public ArrayList<CD> selectCDs() {
        return library.getCatalog().getCDs();
    }
    
    /**
     * Trae lista de dvds disponibles en la biblioteca
     * @return 
     */
    public ArrayList<DVD> selectDVDs() {
        return library.getCatalog().getDVDs();
    }
    
    /**
     * Trae lista de proyectores disponibles en la biblioteca
     * @return 
     */
    public ArrayList<Proyector> selectProyectors() {
        return library.getCatalog().getProyectors();
    }
    
    /**
     * Trae la respuesta de si un id de estudiante esta o no registrado
     * @return 
     */
    public Student isStudent(String id) {        
        return library.findStudent(id);
    }
    
    /**
     * Suma o resta días a una fecha
     * @param date
     * @param days
     * @return 
     */
    public Date addDaysDate(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date); // Configuramos la fecha que se recibe
        calendar.add(Calendar.DAY_OF_YEAR, days);  // numero de días a añadir, o restar en caso de días<0
        return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos
    }

    /**
     * Busca y colocan como no disponible algun material de la biblioteca
     * @param valueOf 
     */
    public void lend(String idMaterial, Student student) {
        library.findMaterial(idMaterial).setStatus(false);
        library.getLoans().add(new Loan(library.getLoans().size()+1, new Date(), addDaysDate(new Date(), library.getLoanDays()), student, library.findMaterial(idMaterial)));
    }
        
}
