/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliotech;

import Interface.Main;
import controller.CtrlLoans;
import controller.CtrlRegisters;
import domain.Library;

/**
 *
 * @author geovanny
 */
public class BiblioTech {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Library library = new Library();
        library.CargarDatos();
        library.print();
        CtrlLoans ctrlLoans = new CtrlLoans(library);
        CtrlRegisters ctrlRegisterMaterial = new CtrlRegisters(library);
        Main viewMain = new Main(ctrlRegisterMaterial, ctrlLoans);
        viewMain.setVisible(true);
    }
    
}
